//
//  Weather.swift
//  SegundoObligatorio
//
//  Created by SP21 on 20/5/16.
//  Copyright © 2016 Universidad Catolica. All rights reserved.
//

import Foundation
import ObjectMapper


class Weather : Mappable {
    var icon: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.icon <- map["icon"]
    }
}