//
//  Utils.swift
//  SegundoObligatorio
//
//  Created by SP22 on 26/5/16.
//  Copyright © 2016 Universidad Catolica. All rights reserved.
//

import Foundation

class Utils {
    
    static func getDayOfWeekFromDT(dt:Double)->String? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        let dateFormatter2 = NSDateFormatter()
        dateFormatter2.dateFormat = "EEE"
        
        return dateFormatter2.stringFromDate(NSDate(timeIntervalSince1970: dt))
    }
}
