//
//  DayCollectionViewCell.swift
//  SegundoObligatorio
//
//  Created by SP21 on 20/5/16.
//  Copyright © 2016 Universidad Catolica. All rights reserved.
//

import UIKit

class DayCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var measureLabel: UILabel!
    
}
