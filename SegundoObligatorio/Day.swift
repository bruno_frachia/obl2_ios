//
//  Day.swift
//  SegundoObligatorio
//
//  Created by SP21 on 20/5/16.
//  Copyright © 2016 Universidad Catolica. All rights reserved.
//

import Foundation
import ObjectMapper

class Day : Mappable {
    var dt: Double?
    var avgTemp: Float?
    var weatherList: [Weather]?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.dt <- map["dt"]
        self.avgTemp <- map["temp.day"]
        self.weatherList <- map["weather"]
    }
}