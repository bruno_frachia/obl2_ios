//
//  APIClient.swift
//  SegundoObligatorio
//
//  Created by SP22 on 18/5/16.
//  Copyright © 2016 Universidad Catolica. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper
import Alamofire
import CoreLocation



class APIClient {
    
    static let sharedClient = APIClient()
    
    private let baseURL = "http://api.openweathermap.org/data/2.5/"
    
    private let method = "forecast/"
    
    private let methodType = "daily"
    
    private let days = "7"
    
    private let apiKey = "026e8dd2cd41329b0dceb0150db12983"
    
    private init() {
        
    }
    
    func forecastOnCompletion(onCompletion: (forecasts: Forecast?, error: NSError?) -> Void, location: CLLocation, dataUnit: String) {
        
        let completeUrl = self.baseURL +  method + methodType + "?APPID=\(apiKey)&lat=\(location.coordinate.latitude)&lon=\(location.coordinate.longitude)&cnt=\(days)&units=\(dataUnit)"
        
        Alamofire.request(.GET, completeUrl).validate().responseJSON { (response: Response<AnyObject, NSError>) -> Void in
            
            switch response.result {
                
            case .Failure(let error):
                onCompletion(forecasts: nil, error: error)
            case .Success(let value):
                
                if let forecasts = Mapper<Forecast>().map(value) {
                    onCompletion(forecasts: forecasts, error: nil)
                }else {
                    onCompletion(forecasts: nil, error: NSError(domain: "MyApp", code: 9999, userInfo: [NSLocalizedDescriptionKey: "Fallo el mapeo"]))
                }
            }
            
        }
    }
    
    
    
}
