//
//  ViewController.swift
//  SegundoObligatorio
//
//  Created by Diego Pais on 5/18/16.
//  Copyright © 2016 Universidad Catolica. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var weatherIconLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var avgTempLabel: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var measureLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var mainView: UIView!
    
    
    var defaults : NSUserDefaults?
    var measure: String?
    let locationManager = CLLocationManager()
    var forecast: Forecast? {
        didSet {
            self.collectionView?.reloadData()
        }
    }
    
    var lastRowShown = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //Load defaults
        self.defaults = NSUserDefaults.standardUserDefaults()
        
        self.measure = "metric"

        self.collectionView.backgroundColor = UIColor.clearColor()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        //set timer target
        NSTimer.scheduledTimerWithTimeInterval(20, target: self, selector: Selector("handleTimer:"), userInfo: nil, repeats: true)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        updateForecast()
        
    }
    
    func updateForecast() {
        if let defaultMeasure = defaults!.stringForKey("tempUnit") {
            self.measure = defaultMeasure
        }
        else {
            self.defaults!.setObject("tempUnit", forKey: "metric")
            self.measure = "metric"
        }
        
        
        //set measure unit in main page
        if (self.measure == "metric"){
            self.measureLabel.text = "°C"
        }
        else{
            self.measureLabel.text = "°F"
        }
        
        
        if defaults!.boolForKey("currentLocationMode") {
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            }
            else {
                let alertController = UIAlertController(title: "Error", message: "Disabled Location Services", preferredStyle: .Alert)
                let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(defaultAction)
                presentViewController(alertController, animated: true, completion: nil)
            }
            
        }
        else {
            var defaultLatitude = 0.0
            var defaultLongitude = 0.0
            
            if let latitude : Double = defaults!.doubleForKey("latitude"),
                longitude : Double = defaults!.doubleForKey("longitude") {
                    
                    defaultLatitude = latitude
                    defaultLongitude = longitude
                    
            }
            let locationCoords = CLLocation.init(latitude: defaultLatitude, longitude: defaultLongitude)
            callApiMethod(locationCoords, measure: self.measure!)
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        
        let locValue = locations.last!
        callApiMethod(locValue, measure: self.measure!)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Collection view data source
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.forecast!.list!.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("dayCell", forIndexPath: indexPath) as! DayCollectionViewCell
        
        let dateTime = self.forecast!.list![indexPath.item].dt!
        
        cell.dayLabel.text = Utils.getDayOfWeekFromDT(dateTime)
        cell.iconLabel.text = WeatherIcon(condition: 200, iconString: self.forecast!.list![indexPath.item].weatherList![0].icon!).iconText
        cell.tempLabel.text = String(Int(round(self.forecast!.list![indexPath.item].avgTemp!)))
        
        if (self.measure == "metric"){
            cell.measureLabel.text = "°C"
        }
        else{
            cell.measureLabel.text = "°F"
        }
        
        
        
        return cell
    }
    
    func handleTimer(timer: NSTimer){
        if defaults!.boolForKey("currentLocationMode"){
            self.updateForecast()
        }
    }
    
    // MARK: - API methods
    
    func callApiMethod(location: CLLocation, measure: String) {
        //show loading indicator.
        self.indicator.startAnimating()
        
        //call to API REST to get the forcast for next 7 days.
        APIClient.sharedClient.forecastOnCompletion({ (forecast, error) -> Void in
            
            if let forecast = forecast {
                
                let myDay = forecast.list!.removeAtIndex(0)
                self.weatherIconLabel.text =  WeatherIcon(condition: 200, iconString: myDay.weatherList![0].icon!).iconText
                self.avgTempLabel.text = String(Int(round(myDay.avgTemp!)))
                self.cityNameLabel.text = forecast.name
                
                UIView.animateWithDuration(1.0, animations: {
                    self.cityNameLabel.alpha = 1.0
                    self.weatherIconLabel.alpha = 1.0
                    self.avgTempLabel.alpha = 1.0
                    self.measureLabel.alpha = 1.0
                })
                self.forecast = forecast
                self.collectionView.delegate = self
                self.collectionView.dataSource = self
            }
            else {
                
                let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .Alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            
            self.indicator.stopAnimating()
            
            }, location: location, dataUnit: measure)
    }
    
    // MARK: - Collection view delegate

    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        //Animate cell when is shown by first time
        if(indexPath.row > lastRowShown) {
            lastRowShown = indexPath.row
            cell.transform = CGAffineTransformMakeScale(0, 0)
            
            var delay = 0.0
            
            //Only the cells that are shown at first (each cell has width = screenWidth/4)
            //will have a delayed animation (the others will animate when appear on screen)
            if(indexPath.row <= 3) {
                delay = Double(indexPath.row)/6
            }
            
            UIView.animateWithDuration(1, delay: delay, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .CurveEaseInOut, animations: { () -> Void in
                cell.transform = CGAffineTransformIdentity
                }, completion: nil)
        }
        
    }
    
    // MARK: - Collection view flow layout delegate

    //Make spacing between cells 0
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) ->
        //Make cells' width be 1/4 of screen's width
        CGSize {
        return CGSizeMake(UIScreen.mainScreen().bounds.width/4, 150)
    }
    
    // MARK: - Status bar color

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        //Set status bar style to Light so it will contrast with the background
        return UIStatusBarStyle.LightContent
        
    }

}

