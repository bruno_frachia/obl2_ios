//
//  SettingsViewController.swift
//  SegundoObligatorio
//
//  Created by SP21 on 27/5/16.
//  Copyright © 2016 Universidad Catolica. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation



class SettingsViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var lineViewHeight: NSLayoutConstraint!
    @IBOutlet weak var switchButton: UISwitch!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    
    let locationManager = CLLocationManager()
    var defaults : NSUserDefaults?
    
    var annotationLatitude : CLLocationDegrees?
    var annotationLongitude : CLLocationDegrees?
    var currentMeasure : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.defaults = NSUserDefaults.standardUserDefaults()
        
        loadDefaultsInfo()
        
        //Resize separator view
        self.lineViewHeight.constant=0.5
        
        //Location Manager
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        
        switchAction(self)
        
        
        //Gesture Recognizer
//        let gestureRecog = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        let gestureRecog = UILongPressGestureRecognizer(target: self, action:"handleLongPress:")
        gestureRecog.minimumPressDuration = 1.5
        gestureRecog.delegate = self
        self.mapView.addGestureRecognizer(gestureRecog)
        
        
        //set timer target
        NSTimer.scheduledTimerWithTimeInterval(20, target: self, selector: Selector("handleTimer:"), userInfo: nil, repeats: true)
        
    }
    
    
    func handleLongPress(gestureRecognizer:UIGestureRecognizer){
        
        //check if current location is disabled
        if  (!self.switchButton.on) {
            //remove previous annotations
            mapView.removeAnnotations(mapView.annotations)
            
            
            //add gesture annotation
            let touchPoint = gestureRecognizer.locationInView(mapView)
            let newCoordinates = mapView.convertPoint(touchPoint, toCoordinateFromView: mapView)
            let annotation = MKPointAnnotation()
            annotation.coordinate = newCoordinates
            mapView.addAnnotation(annotation)
            
            //store latitude and longitude for saving method
            self.annotationLatitude = newCoordinates.latitude
            self.annotationLongitude = newCoordinates.longitude
        }
        
    }
    
    func handleTimer(timer: NSTimer){
        if self.switchButton.on{
            self.switchAction(self)
        }
    }
    
    func loadUserLocation(){
        self.locationManager.startUpdatingLocation()
        self.mapView.showsUserLocation = true
    }

    
    // MARK: - Location Manager Methods
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.loadingActivity.startAnimating()
        let currentLocation = locations.last!
        
        let coord2D = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        
        let coordRegion = MKCoordinateRegion(center: coord2D, span: MKCoordinateSpan(latitudeDelta: 2, longitudeDelta: 2))
        
        self.mapView.setRegion(coordRegion, animated: true)
        
        self.locationManager.stopUpdatingLocation()
        self.loadingActivity.stopAnimating()
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        let alertController = UIAlertController(title: "", message: "Se ha presentado un error al intentar acceder a su ubicación", preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true, completion: nil)
        
        self.switchButton.setOn(false, animated: true)
        self.switchButton.enabled = false
    }
    
    @IBAction func segmentedControlAction(sender: AnyObject) {
        if currentMeasure == "metric"{
            currentMeasure = "Imperial"
            
        }else{
            currentMeasure = "metric"
        }
        
    }

    @IBAction func switchAction(sender: AnyObject) {
        
        if switchButton.on {
            loadUserLocation()
        }else{
            
            // if there is no annotation it will load map in default location (Uruguay)
            var defaultCoord:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude:-33.428595, longitude:  -56.021169)
            
            if let defLatitude : Double = annotationLatitude ,
            defLongitude : Double = annotationLongitude {
                defaultCoord = CLLocationCoordinate2D(latitude: defLatitude, longitude: defLongitude)
            }
            
            let regionRadius: CLLocationDistance = 600000
            let region = MKCoordinateRegionMakeWithDistance(defaultCoord, regionRadius * 2.0, regionRadius * 2.0)
            self.mapView.setRegion(region, animated: true)
            
            //add annotation
            mapView.removeAnnotations(mapView.annotations)
            let annotation = MKPointAnnotation()
            annotation.coordinate = defaultCoord
            mapView.addAnnotation(annotation)
        }
        
    }
    
    
    
    func loadDefaultsInfo(){
    
        //set tempUnit
        if let tempUnit: String = defaults!.stringForKey("tempUnit") {
            self.currentMeasure = tempUnit
        }
        
        //set switchButton status
        if let switchStatus: Bool = defaults!.boolForKey("currentLocationMode") {
            switchButton.setOn(switchStatus, animated: false)
        }
        
        //Set segmentedControl. 0 -> metric (C), 1 -> Imperial(F)
        if self.currentMeasure == "metric" {
            self.segmentedControl.selectedSegmentIndex = 0
        }
        else {
            self.segmentedControl.selectedSegmentIndex = 1
        }
        
        //set annotation coordinates
        if let latitude: CLLocationDegrees = defaults!.doubleForKey("latitude") {
            annotationLatitude = latitude
        }
        
        if let longitude: CLLocationDegrees = defaults!.doubleForKey("longitude") {
            annotationLongitude = longitude
        }

    }
    
    
    
    @IBAction func dismissAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func saveAction(sender: AnyObject) {
         //add latitude and longitude of annotation to defaults
        if let annotationLatitude : Double = self.annotationLatitude {
             self.defaults!.setObject(annotationLatitude, forKey: "latitude")
        }
        
        if let annotationLongitude : Double = self.annotationLongitude {
            self.defaults!.setObject(annotationLongitude, forKey: "longitude")
        }
        
        //add switchButton status to defaults
        self.defaults!.setObject(switchButton.on, forKey: "currentLocationMode")
      
        //add tempUnit to defaults
        if let currentMeasure : String = self.currentMeasure {
             self.defaults!.setObject(currentMeasure, forKey: "tempUnit")
        }
        
        
        //notify user about successful changes and return to main view
        let alertController = UIAlertController(title: "", message: "Los cambios se realizaron exitosamente", preferredStyle: .Alert)
        
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        presentViewController(alertController, animated: true, completion: nil)
        
        
    }
    
    
    
}
